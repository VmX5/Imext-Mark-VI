﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Form1))
        Me.SlickBlueTabControl1 = New Imext_Mark_VI.SlickBlueTabControl()
        Me.TabPage1 = New System.Windows.Forms.TabPage()
        Me.VelocityButton7 = New Imext_Mark_VI.VelocityButton()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.VelocityButton2 = New Imext_Mark_VI.VelocityButton()
        Me.VelocityButton1 = New Imext_Mark_VI.VelocityButton()
        Me.panel1 = New Imext_Mark_VI.BorderPanel()
        Me.inputBox = New System.Windows.Forms.TextBox()
        Me.encCharCount = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.BorderPanel1 = New Imext_Mark_VI.BorderPanel()
        Me.VelocityButton6 = New Imext_Mark_VI.VelocityButton()
        Me.BorderPanel5 = New Imext_Mark_VI.BorderPanel()
        Me.txtHeight = New System.Windows.Forms.TextBox()
        Me.BorderPanel4 = New Imext_Mark_VI.BorderPanel()
        Me.txtWidth = New System.Windows.Forms.TextBox()
        Me.VelocityButton5 = New Imext_Mark_VI.VelocityButton()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.TabPage2 = New System.Windows.Forms.TabPage()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.VelocityButton3 = New Imext_Mark_VI.VelocityButton()
        Me.VelocityButton4 = New Imext_Mark_VI.VelocityButton()
        Me.BorderPanel2 = New Imext_Mark_VI.BorderPanel()
        Me.outputBox = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.BorderPanel3 = New Imext_Mark_VI.BorderPanel()
        Me.inputImage = New System.Windows.Forms.PictureBox()
        Me.TabPage3 = New System.Windows.Forms.TabPage()
        Me.RichTextBox1 = New System.Windows.Forms.RichTextBox()
        Me.TabPage4 = New System.Windows.Forms.TabPage()
        Me.RichTextBox2 = New System.Windows.Forms.RichTextBox()
        Me.SlickBlueTabControl1.SuspendLayout()
        Me.TabPage1.SuspendLayout()
        Me.panel1.SuspendLayout()
        Me.BorderPanel1.SuspendLayout()
        Me.BorderPanel5.SuspendLayout()
        Me.BorderPanel4.SuspendLayout()
        Me.TabPage2.SuspendLayout()
        Me.BorderPanel2.SuspendLayout()
        Me.BorderPanel3.SuspendLayout()
        CType(Me.inputImage, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabPage3.SuspendLayout()
        Me.TabPage4.SuspendLayout()
        Me.SuspendLayout()
        '
        'SlickBlueTabControl1
        '
        Me.SlickBlueTabControl1.Alignment = System.Windows.Forms.TabAlignment.Left
        Me.SlickBlueTabControl1.Controls.Add(Me.TabPage1)
        Me.SlickBlueTabControl1.Controls.Add(Me.TabPage2)
        Me.SlickBlueTabControl1.Controls.Add(Me.TabPage3)
        Me.SlickBlueTabControl1.Controls.Add(Me.TabPage4)
        Me.SlickBlueTabControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SlickBlueTabControl1.Font = New System.Drawing.Font("Segoe UI Semilight", 9.0!)
        Me.SlickBlueTabControl1.ItemSize = New System.Drawing.Size(40, 130)
        Me.SlickBlueTabControl1.Location = New System.Drawing.Point(0, 0)
        Me.SlickBlueTabControl1.Multiline = True
        Me.SlickBlueTabControl1.Name = "SlickBlueTabControl1"
        Me.SlickBlueTabControl1.SelectedIndex = 0
        Me.SlickBlueTabControl1.Size = New System.Drawing.Size(706, 195)
        Me.SlickBlueTabControl1.SizeMode = System.Windows.Forms.TabSizeMode.Fixed
        Me.SlickBlueTabControl1.TabIndex = 0
        '
        'TabPage1
        '
        Me.TabPage1.Controls.Add(Me.VelocityButton7)
        Me.TabPage1.Controls.Add(Me.Label8)
        Me.TabPage1.Controls.Add(Me.VelocityButton2)
        Me.TabPage1.Controls.Add(Me.VelocityButton1)
        Me.TabPage1.Controls.Add(Me.panel1)
        Me.TabPage1.Controls.Add(Me.encCharCount)
        Me.TabPage1.Controls.Add(Me.Label1)
        Me.TabPage1.Controls.Add(Me.Label2)
        Me.TabPage1.Controls.Add(Me.BorderPanel1)
        Me.TabPage1.Font = New System.Drawing.Font("Consolas", 9.0!)
        Me.TabPage1.Location = New System.Drawing.Point(134, 4)
        Me.TabPage1.Name = "TabPage1"
        Me.TabPage1.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage1.Size = New System.Drawing.Size(568, 187)
        Me.TabPage1.TabIndex = 0
        Me.TabPage1.Text = "Encrypt"
        Me.TabPage1.UseVisualStyleBackColor = True
        '
        'VelocityButton7
        '
        Me.VelocityButton7.Font = New System.Drawing.Font("Segoe UI Semilight", 9.0!)
        Me.VelocityButton7.ForeColor = System.Drawing.Color.White
        Me.VelocityButton7.Location = New System.Drawing.Point(187, 146)
        Me.VelocityButton7.Name = "VelocityButton7"
        Me.VelocityButton7.Size = New System.Drawing.Size(34, 35)
        Me.VelocityButton7.TabIndex = 23
        Me.VelocityButton7.Text = "RND"
        Me.VelocityButton7.TextAlign = Imext_Mark_VI.Helpers.TxtAlign.Center
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Consolas", 8.0!)
        Me.Label8.Location = New System.Drawing.Point(6, 159)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(115, 13)
        Me.Label8.TabIndex = 22
        Me.Label8.Text = "Max Count  : 90000"
        '
        'VelocityButton2
        '
        Me.VelocityButton2.Font = New System.Drawing.Font("Segoe UI Semilight", 9.0!)
        Me.VelocityButton2.ForeColor = System.Drawing.Color.White
        Me.VelocityButton2.Location = New System.Drawing.Point(315, 146)
        Me.VelocityButton2.Name = "VelocityButton2"
        Me.VelocityButton2.Size = New System.Drawing.Size(80, 35)
        Me.VelocityButton2.TabIndex = 21
        Me.VelocityButton2.Text = "Convert"
        Me.VelocityButton2.TextAlign = Imext_Mark_VI.Helpers.TxtAlign.Center
        '
        'VelocityButton1
        '
        Me.VelocityButton1.Font = New System.Drawing.Font("Segoe UI Semilight", 9.0!)
        Me.VelocityButton1.ForeColor = System.Drawing.Color.White
        Me.VelocityButton1.Location = New System.Drawing.Point(224, 146)
        Me.VelocityButton1.Name = "VelocityButton1"
        Me.VelocityButton1.Size = New System.Drawing.Size(85, 35)
        Me.VelocityButton1.TabIndex = 20
        Me.VelocityButton1.Text = "Save Output"
        Me.VelocityButton1.TextAlign = Imext_Mark_VI.Helpers.TxtAlign.Center
        '
        'panel1
        '
        Me.panel1.Controls.Add(Me.inputBox)
        Me.panel1.Location = New System.Drawing.Point(9, 23)
        Me.panel1.Name = "panel1"
        Me.panel1.Padding = New System.Windows.Forms.Padding(3, 4, 3, 1)
        Me.panel1.Size = New System.Drawing.Size(386, 116)
        Me.panel1.TabIndex = 19
        '
        'inputBox
        '
        Me.inputBox.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.inputBox.Dock = System.Windows.Forms.DockStyle.Fill
        Me.inputBox.Location = New System.Drawing.Point(3, 4)
        Me.inputBox.MaxLength = 1000000000
        Me.inputBox.Multiline = True
        Me.inputBox.Name = "inputBox"
        Me.inputBox.ScrollBars = System.Windows.Forms.ScrollBars.Both
        Me.inputBox.Size = New System.Drawing.Size(380, 111)
        Me.inputBox.TabIndex = 0
        '
        'encCharCount
        '
        Me.encCharCount.AutoSize = True
        Me.encCharCount.Font = New System.Drawing.Font("Consolas", 8.0!)
        Me.encCharCount.Location = New System.Drawing.Point(6, 146)
        Me.encCharCount.Name = "encCharCount"
        Me.encCharCount.Size = New System.Drawing.Size(91, 13)
        Me.encCharCount.TabIndex = 17
        Me.encCharCount.Text = "Char Count : 0"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(6, 6)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(42, 14)
        Me.Label1.TabIndex = 15
        Me.Label1.Text = "Input"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(398, 6)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(133, 14)
        Me.Label2.TabIndex = 16
        Me.Label2.Text = "Output Information"
        '
        'BorderPanel1
        '
        Me.BorderPanel1.Controls.Add(Me.VelocityButton6)
        Me.BorderPanel1.Controls.Add(Me.BorderPanel5)
        Me.BorderPanel1.Controls.Add(Me.BorderPanel4)
        Me.BorderPanel1.Controls.Add(Me.VelocityButton5)
        Me.BorderPanel1.Controls.Add(Me.Label7)
        Me.BorderPanel1.Controls.Add(Me.Label6)
        Me.BorderPanel1.Location = New System.Drawing.Point(401, 23)
        Me.BorderPanel1.Name = "BorderPanel1"
        Me.BorderPanel1.Padding = New System.Windows.Forms.Padding(4)
        Me.BorderPanel1.Size = New System.Drawing.Size(158, 158)
        Me.BorderPanel1.TabIndex = 14
        '
        'VelocityButton6
        '
        Me.VelocityButton6.Font = New System.Drawing.Font("Segoe UI Semilight", 9.0!)
        Me.VelocityButton6.ForeColor = System.Drawing.Color.White
        Me.VelocityButton6.Location = New System.Drawing.Point(17, 118)
        Me.VelocityButton6.Name = "VelocityButton6"
        Me.VelocityButton6.Size = New System.Drawing.Size(55, 33)
        Me.VelocityButton6.TabIndex = 5
        Me.VelocityButton6.Text = "Show"
        Me.VelocityButton6.TextAlign = Imext_Mark_VI.Helpers.TxtAlign.Center
        '
        'BorderPanel5
        '
        Me.BorderPanel5.Controls.Add(Me.txtHeight)
        Me.BorderPanel5.Location = New System.Drawing.Point(17, 79)
        Me.BorderPanel5.Name = "BorderPanel5"
        Me.BorderPanel5.Padding = New System.Windows.Forms.Padding(3, 4, 3, 1)
        Me.BorderPanel5.Size = New System.Drawing.Size(123, 23)
        Me.BorderPanel5.TabIndex = 4
        '
        'txtHeight
        '
        Me.txtHeight.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtHeight.Dock = System.Windows.Forms.DockStyle.Fill
        Me.txtHeight.Location = New System.Drawing.Point(3, 4)
        Me.txtHeight.Name = "txtHeight"
        Me.txtHeight.Size = New System.Drawing.Size(117, 15)
        Me.txtHeight.TabIndex = 3
        Me.txtHeight.Text = "150"
        '
        'BorderPanel4
        '
        Me.BorderPanel4.Controls.Add(Me.txtWidth)
        Me.BorderPanel4.Location = New System.Drawing.Point(17, 30)
        Me.BorderPanel4.Name = "BorderPanel4"
        Me.BorderPanel4.Padding = New System.Windows.Forms.Padding(3, 4, 3, 1)
        Me.BorderPanel4.Size = New System.Drawing.Size(123, 23)
        Me.BorderPanel4.TabIndex = 1
        '
        'txtWidth
        '
        Me.txtWidth.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtWidth.Dock = System.Windows.Forms.DockStyle.Fill
        Me.txtWidth.Location = New System.Drawing.Point(3, 4)
        Me.txtWidth.Name = "txtWidth"
        Me.txtWidth.Size = New System.Drawing.Size(117, 15)
        Me.txtWidth.TabIndex = 3
        Me.txtWidth.Text = "150"
        '
        'VelocityButton5
        '
        Me.VelocityButton5.Font = New System.Drawing.Font("Segoe UI Semilight", 9.0!)
        Me.VelocityButton5.ForeColor = System.Drawing.Color.White
        Me.VelocityButton5.Location = New System.Drawing.Point(78, 118)
        Me.VelocityButton5.Name = "VelocityButton5"
        Me.VelocityButton5.Size = New System.Drawing.Size(62, 33)
        Me.VelocityButton5.TabIndex = 2
        Me.VelocityButton5.Text = "Set"
        Me.VelocityButton5.TextAlign = Imext_Mark_VI.Helpers.TxtAlign.Center
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(14, 62)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(49, 14)
        Me.Label7.TabIndex = 1
        Me.Label7.Text = "Height"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(14, 13)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(42, 14)
        Me.Label6.TabIndex = 0
        Me.Label6.Text = "Width"
        '
        'TabPage2
        '
        Me.TabPage2.Controls.Add(Me.Label3)
        Me.TabPage2.Controls.Add(Me.VelocityButton3)
        Me.TabPage2.Controls.Add(Me.VelocityButton4)
        Me.TabPage2.Controls.Add(Me.BorderPanel2)
        Me.TabPage2.Controls.Add(Me.Label4)
        Me.TabPage2.Controls.Add(Me.Label5)
        Me.TabPage2.Controls.Add(Me.BorderPanel3)
        Me.TabPage2.Font = New System.Drawing.Font("Consolas", 9.0!)
        Me.TabPage2.Location = New System.Drawing.Point(134, 4)
        Me.TabPage2.Name = "TabPage2"
        Me.TabPage2.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage2.Size = New System.Drawing.Size(568, 187)
        Me.TabPage2.TabIndex = 1
        Me.TabPage2.Text = "Decrypt"
        Me.TabPage2.UseVisualStyleBackColor = True
        '
        'Label3
        '
        Me.Label3.Font = New System.Drawing.Font("Consolas", 8.0!)
        Me.Label3.Location = New System.Drawing.Point(417, 140)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(140, 13)
        Me.Label3.TabIndex = 25
        Me.Label3.Text = "Char Count : 0"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'VelocityButton3
        '
        Me.VelocityButton3.Font = New System.Drawing.Font("Segoe UI Semilight", 9.0!)
        Me.VelocityButton3.ForeColor = System.Drawing.Color.White
        Me.VelocityButton3.Location = New System.Drawing.Point(266, 144)
        Me.VelocityButton3.Name = "VelocityButton3"
        Me.VelocityButton3.Size = New System.Drawing.Size(80, 35)
        Me.VelocityButton3.TabIndex = 29
        Me.VelocityButton3.Text = "Convert"
        Me.VelocityButton3.TextAlign = Imext_Mark_VI.Helpers.TxtAlign.Center
        '
        'VelocityButton4
        '
        Me.VelocityButton4.Font = New System.Drawing.Font("Segoe UI Semilight", 9.0!)
        Me.VelocityButton4.ForeColor = System.Drawing.Color.White
        Me.VelocityButton4.Location = New System.Drawing.Point(175, 144)
        Me.VelocityButton4.Name = "VelocityButton4"
        Me.VelocityButton4.Size = New System.Drawing.Size(85, 35)
        Me.VelocityButton4.TabIndex = 28
        Me.VelocityButton4.Text = "Import"
        Me.VelocityButton4.TextAlign = Imext_Mark_VI.Helpers.TxtAlign.Center
        '
        'BorderPanel2
        '
        Me.BorderPanel2.Controls.Add(Me.outputBox)
        Me.BorderPanel2.Location = New System.Drawing.Point(171, 21)
        Me.BorderPanel2.Name = "BorderPanel2"
        Me.BorderPanel2.Padding = New System.Windows.Forms.Padding(3, 4, 3, 1)
        Me.BorderPanel2.Size = New System.Drawing.Size(386, 116)
        Me.BorderPanel2.TabIndex = 27
        '
        'outputBox
        '
        Me.outputBox.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.outputBox.Dock = System.Windows.Forms.DockStyle.Fill
        Me.outputBox.Location = New System.Drawing.Point(3, 4)
        Me.outputBox.MaxLength = 1000000000
        Me.outputBox.Multiline = True
        Me.outputBox.Name = "outputBox"
        Me.outputBox.ScrollBars = System.Windows.Forms.ScrollBars.Both
        Me.outputBox.Size = New System.Drawing.Size(380, 111)
        Me.outputBox.TabIndex = 0
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(5, 6)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(42, 14)
        Me.Label4.TabIndex = 23
        Me.Label4.Text = "Input"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(168, 6)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(49, 14)
        Me.Label5.TabIndex = 24
        Me.Label5.Text = "Output"
        '
        'BorderPanel3
        '
        Me.BorderPanel3.Controls.Add(Me.inputImage)
        Me.BorderPanel3.Location = New System.Drawing.Point(8, 21)
        Me.BorderPanel3.Name = "BorderPanel3"
        Me.BorderPanel3.Padding = New System.Windows.Forms.Padding(4)
        Me.BorderPanel3.Size = New System.Drawing.Size(158, 158)
        Me.BorderPanel3.TabIndex = 22
        '
        'inputImage
        '
        Me.inputImage.Dock = System.Windows.Forms.DockStyle.Fill
        Me.inputImage.Location = New System.Drawing.Point(4, 4)
        Me.inputImage.Name = "inputImage"
        Me.inputImage.Size = New System.Drawing.Size(150, 150)
        Me.inputImage.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.inputImage.TabIndex = 0
        Me.inputImage.TabStop = False
        '
        'TabPage3
        '
        Me.TabPage3.Controls.Add(Me.RichTextBox1)
        Me.TabPage3.Font = New System.Drawing.Font("Consolas", 9.0!)
        Me.TabPage3.Location = New System.Drawing.Point(134, 4)
        Me.TabPage3.Name = "TabPage3"
        Me.TabPage3.Size = New System.Drawing.Size(568, 187)
        Me.TabPage3.TabIndex = 2
        Me.TabPage3.Text = "Update Notes"
        Me.TabPage3.UseVisualStyleBackColor = True
        '
        'RichTextBox1
        '
        Me.RichTextBox1.Location = New System.Drawing.Point(9, 9)
        Me.RichTextBox1.Name = "RichTextBox1"
        Me.RichTextBox1.Size = New System.Drawing.Size(551, 170)
        Me.RichTextBox1.TabIndex = 0
        Me.RichTextBox1.Text = "Version : Imext Mark VI" & Global.Microsoft.VisualBasic.ChrW(10) & Global.Microsoft.VisualBasic.ChrW(10) & "- Now uses the Alpha value of a pixel, allowing 133% mor" & _
    "e storage." & Global.Microsoft.VisualBasic.ChrW(10) & Global.Microsoft.VisualBasic.ChrW(10) & "- Dynamic Sizing, images can now be more than 150x150 allowing longe" & _
    "r strings to be stored."
        '
        'TabPage4
        '
        Me.TabPage4.Controls.Add(Me.RichTextBox2)
        Me.TabPage4.Location = New System.Drawing.Point(134, 4)
        Me.TabPage4.Name = "TabPage4"
        Me.TabPage4.Size = New System.Drawing.Size(568, 187)
        Me.TabPage4.TabIndex = 3
        Me.TabPage4.Text = "About"
        Me.TabPage4.UseVisualStyleBackColor = True
        '
        'RichTextBox2
        '
        Me.RichTextBox2.Font = New System.Drawing.Font("Consolas", 9.0!)
        Me.RichTextBox2.Location = New System.Drawing.Point(7, 8)
        Me.RichTextBox2.Name = "RichTextBox2"
        Me.RichTextBox2.Size = New System.Drawing.Size(553, 171)
        Me.RichTextBox2.TabIndex = 0
        Me.RichTextBox2.Text = resources.GetString("RichTextBox2.Text")
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(706, 195)
        Me.Controls.Add(Me.SlickBlueTabControl1)
        Me.Name = "Form1"
        Me.Text = "Imext Mark VI"
        Me.SlickBlueTabControl1.ResumeLayout(False)
        Me.TabPage1.ResumeLayout(False)
        Me.TabPage1.PerformLayout()
        Me.panel1.ResumeLayout(False)
        Me.panel1.PerformLayout()
        Me.BorderPanel1.ResumeLayout(False)
        Me.BorderPanel1.PerformLayout()
        Me.BorderPanel5.ResumeLayout(False)
        Me.BorderPanel5.PerformLayout()
        Me.BorderPanel4.ResumeLayout(False)
        Me.BorderPanel4.PerformLayout()
        Me.TabPage2.ResumeLayout(False)
        Me.TabPage2.PerformLayout()
        Me.BorderPanel2.ResumeLayout(False)
        Me.BorderPanel2.PerformLayout()
        Me.BorderPanel3.ResumeLayout(False)
        CType(Me.inputImage, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabPage3.ResumeLayout(False)
        Me.TabPage4.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents SlickBlueTabControl1 As Imext_Mark_VI.SlickBlueTabControl
    Friend WithEvents TabPage2 As System.Windows.Forms.TabPage
    Friend WithEvents TabPage3 As System.Windows.Forms.TabPage
    Friend WithEvents TabPage4 As System.Windows.Forms.TabPage
    Friend WithEvents RichTextBox1 As System.Windows.Forms.RichTextBox
    Friend WithEvents TabPage1 As System.Windows.Forms.TabPage
    Friend WithEvents VelocityButton2 As Imext_Mark_VI.VelocityButton
    Friend WithEvents VelocityButton1 As Imext_Mark_VI.VelocityButton
    Friend WithEvents panel1 As Imext_Mark_VI.BorderPanel
    Friend WithEvents encCharCount As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents BorderPanel1 As Imext_Mark_VI.BorderPanel
    Friend WithEvents VelocityButton3 As Imext_Mark_VI.VelocityButton
    Friend WithEvents VelocityButton4 As Imext_Mark_VI.VelocityButton
    Friend WithEvents BorderPanel2 As Imext_Mark_VI.BorderPanel
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents BorderPanel3 As Imext_Mark_VI.BorderPanel
    Friend WithEvents inputImage As System.Windows.Forms.PictureBox
    Friend WithEvents inputBox As System.Windows.Forms.TextBox
    Friend WithEvents outputBox As System.Windows.Forms.TextBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Private WithEvents BorderPanel5 As Imext_Mark_VI.BorderPanel
    Friend WithEvents txtHeight As System.Windows.Forms.TextBox
    Private WithEvents BorderPanel4 As Imext_Mark_VI.BorderPanel
    Friend WithEvents txtWidth As System.Windows.Forms.TextBox
    Friend WithEvents VelocityButton5 As Imext_Mark_VI.VelocityButton
    Friend WithEvents VelocityButton6 As Imext_Mark_VI.VelocityButton
    Friend WithEvents RichTextBox2 As System.Windows.Forms.RichTextBox
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents VelocityButton7 As Imext_Mark_VI.VelocityButton

End Class
