﻿Imports System.Text

Public Class Form1
    Dim engine As Imext = New Imext

    Private Sub VelocityButton2_Click(sender As Object, e As EventArgs) Handles VelocityButton2.Click  
        Dim asAscii As String = Encoding.ASCII.GetString(Encoding.Convert(Encoding.UTF8, Encoding.GetEncoding(Encoding.ASCII.EncodingName, New EncoderReplacementFallback(String.Empty), New DecoderExceptionFallback()), Encoding.UTF8.GetBytes(inputBox.Text)))

        output.PictureBox1.Image = engine.TextToImage(asAscii, output.PictureBox1.Size)
        output.Show()
    End Sub

    Private Sub VelocityButton1_Click(sender As Object, e As EventArgs) Handles VelocityButton1.Click
        Dim nSfd As New SaveFileDialog
        nSfd.Filter = "PNG Image|*.png|JPEG Image|*.jpeg"
        If nSfd.ShowDialog = Windows.Forms.DialogResult.OK Then
            output.PictureBox1.Image.Save(nSfd.FileName)
        End If
    End Sub

    Private Sub VelocityButton4_Click(sender As Object, e As EventArgs) Handles VelocityButton4.Click
        Dim nOfd As New OpenFileDialog
        nOfd.Filter = "PNG Image|*.png|JPEG Image|*.jpeg"
        If nOfd.ShowDialog = Windows.Forms.DialogResult.OK Then
            inputImage.Image = Image.FromFile(nOfd.FileName)
            inputImage.Tag = nOfd.FileName.ToString
        End If
    End Sub

    Private Sub VelocityButton3_Click(sender As Object, e As EventArgs) Handles VelocityButton3.Click
        outputBox.Text = engine.ImageToText(inputImage.Tag)
    End Sub

    Private Sub inputBox_TextChanged(sender As Object, e As EventArgs) Handles inputBox.TextChanged
        encCharCount.Text = "Char Count : " & inputBox.Text.Length.ToString
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs)
        output.Show()
    End Sub

    Private Sub VelocityButton5_Click(sender As Object, e As EventArgs) Handles VelocityButton5.Click
        output.Size = New Size(Convert.ToInt32(txtWidth.Text) + 16, Convert.ToInt32(txtHeight.Text) + 39)
    End Sub

    Private Sub outputBox_TextChanged(sender As Object, e As EventArgs) Handles outputBox.TextChanged
        Label3.Text = "Char Count : " & outputBox.Text.Length.ToString
    End Sub

    Private Sub VelocityButton6_Click(sender As Object, e As EventArgs) Handles VelocityButton6.Click
        output.Show()
    End Sub

    Private Sub RichTextBox2_TextChanged(sender As Object, e As EventArgs) Handles RichTextBox2.TextChanged

    End Sub

    Private Sub Label8_Click(sender As Object, e As EventArgs) Handles Label8.Click

    End Sub

    Private Sub txtWidth_TextChanged(sender As Object, e As EventArgs) Handles txtWidth.TextChanged

    End Sub

    Private Sub TabPage1_Click(sender As Object, e As EventArgs) Handles TabPage1.Click

    End Sub

    Private Sub VelocityButton7_Click(sender As Object, e As EventArgs) Handles VelocityButton7.Click
        Dim lorem As Ipsum = New Ipsum
        Dim wordCount As Integer = 1000
        Try
            wordCount = Convert.ToInt32(inputBox.Text)
        Catch
        End Try
        inputBox.Text = lorem.GetWords(wordCount)
    End Sub
End Class

Public Class Ipsum
    Private words As String() = New String() {"consetetur", "sadipscing", "elitr", "sed", "diam", "nonumy", _
        "eirmod", "tempor", "invidunt", "ut", "labore", "et", _
        "dolore", "magna", "aliquyam", "erat", "sed", "diam", _
        "voluptua", "at", "vero", "eos", "et", "accusam", _
        "et", "justo", "duo", "dolores", "et", "ea", _
        "rebum", "stet", "clita", "kasd", "gubergren", "no", _
        "sea", "takimata", "sanctus", "est", "lorem", "ipsum", _
        "dolor", "sit", "amet", "lorem", "ipsum", "dolor", _
        "sit", "amet", "consetetur", "sadipscing", "elitr", "sed", _
        "diam", "nonumy", "eirmod", "tempor", "invidunt", "ut", _
        "labore", "et", "dolore", "magna", "aliquyam", "erat", _
        "sed", "diam", "voluptua", "at", "vero", "eos", _
        "et", "accusam", "et", "justo", "duo", "dolores", _
        "et", "ea", "rebum", "stet", "clita", "kasd", _
        "gubergren", "no", "sea", "takimata", "sanctus", "est", _
        "lorem", "ipsum", "dolor", "sit", "amet", "lorem", _
        "ipsum", "dolor", "sit", "amet", "consetetur", "sadipscing", _
        "elitr", "sed", "diam", "nonumy", "eirmod", "tempor", _
        "invidunt", "ut", "labore", "et", "dolore", "magna", _
        "aliquyam", "erat", "sed", "diam", "voluptua", "at", _
        "vero", "eos", "et", "accusam", "et", "justo", _
        "duo", "dolores", "et", "ea", "rebum", "stet", _
        "clita", "kasd", "gubergren", "no", "sea", "takimata", _
        "sanctus", "est", "lorem", "ipsum", "dolor", "sit", _
        "amet", "duis", "autem", "vel", "eum", "iriure", _
        "dolor", "in", "hendrerit", "in", "vulputate", "velit", _
        "esse", "molestie", "consequat", "vel", "illum", "dolore", _
        "eu", "feugiat", "nulla", "facilisis", "at", "vero", _
        "eros", "et", "accumsan", "et", "iusto", "odio", _
        "dignissim", "qui", "blandit", "praesent", "luptatum", "zzril", _
        "delenit", "augue", "duis", "dolore", "te", "feugait", _
        "nulla", "facilisi", "lorem", "ipsum", "dolor", "sit", _
        "amet", "consectetuer", "adipiscing", "elit", "sed", "diam", _
        "nonummy", "nibh", "euismod", "tincidunt", "ut", "laoreet", _
        "dolore", "magna", "aliquam", "erat", "volutpat", "ut", _
        "wisi", "enim", "ad", "minim", "veniam", "quis", _
        "nostrud", "exerci", "tation", "ullamcorper", "suscipit", "lobortis", _
        "nisl", "ut", "aliquip", "ex", "ea", "commodo", _
        "consequat", "duis", "autem", "vel", "eum", "iriure", _
        "dolor", "in", "hendrerit", "in", "vulputate", "velit", _
        "esse", "molestie", "consequat", "vel", "illum", "dolore", _
        "eu", "feugiat", "nulla", "facilisis", "at", "vero", _
        "eros", "et", "accumsan", "et", "iusto", "odio", _
        "dignissim", "qui", "blandit", "praesent", "luptatum", "zzril", _
        "delenit", "augue", "duis", "dolore", "te", "feugait", _
        "nulla", "facilisi", "nam", "liber", "tempor", "cum", _
        "soluta", "nobis", "eleifend", "option", "congue", "nihil", _
        "imperdiet", "doming", "id", "quod", "mazim", "placerat", _
        "facer", "possim", "assum", "lorem", "ipsum", "dolor", _
        "sit", "amet", "consectetuer", "adipiscing", "elit", "sed", _
        "diam", "nonummy", "nibh", "euismod", "tincidunt", "ut", _
        "laoreet", "dolore", "magna", "aliquam", "erat", "volutpat", _
        "ut", "wisi", "enim", "ad", "minim", "veniam", _
        "quis", "nostrud", "exerci", "tation", "ullamcorper", "suscipit", _
        "lobortis", "nisl", "ut", "aliquip", "ex", "ea", _
        "commodo", "consequat", "duis", "autem", "vel", "eum", _
        "iriure", "dolor", "in", "hendrerit", "in", "vulputate", _
        "velit", "esse", "molestie", "consequat", "vel", "illum", _
        "dolore", "eu", "feugiat", "nulla", "facilisis", "at", _
        "vero", "eos", "et", "accusam", "et", "justo", _
        "duo", "dolores", "et", "ea", "rebum", "stet", _
        "clita", "kasd", "gubergren", "no", "sea", "takimata", _
        "sanctus", "est", "lorem", "ipsum", "dolor", "sit", _
        "amet", "lorem", "ipsum", "dolor", "sit", "amet", _
        "consetetur", "sadipscing", "elitr", "sed", "diam", "nonumy", _
        "eirmod", "tempor", "invidunt", "ut", "labore", "et", _
        "dolore", "magna", "aliquyam", "erat", "sed", "diam", _
        "voluptua", "at", "vero", "eos", "et", "accusam", _
        "et", "justo", "duo", "dolores", "et", "ea", _
        "rebum", "stet", "clita", "kasd", "gubergren", "no", _
        "sea", "takimata", "sanctus", "est", "lorem", "ipsum", _
        "dolor", "sit", "amet", "lorem", "ipsum", "dolor", _
        "sit", "amet", "consetetur", "sadipscing", "elitr", "at", _
        "accusam", "aliquyam", "diam", "diam", "dolore", "dolores", _
        "duo", "eirmod", "eos", "erat", "et", "nonumy", _
        "sed", "tempor", "et", "et", "invidunt", "justo", _
        "labore", "stet", "clita", "ea", "et", "gubergren", _
        "kasd", "magna", "no", "rebum", "sanctus", "sea", _
        "sed", "takimata", "ut", "vero", "voluptua", "est", _
        "lorem", "ipsum", "dolor", "sit", "amet", "lorem", _
        "ipsum", "dolor", "sit", "amet", "consetetur", "sadipscing", _
        "elitr", "sed", "diam", "nonumy", "eirmod", "tempor", _
        "invidunt", "ut", "labore", "et", "dolore", "magna", _
        "aliquyam", "erat", "consetetur", "sadipscing", "elitr", "sed", _
        "diam", "nonumy", "eirmod", "tempor", "invidunt", "ut", _
        "labore", "et", "dolore", "magna", "aliquyam", "erat", _
        "sed", "diam", "voluptua", "at", "vero", "eos", _
        "et", "accusam", "et", "justo", "duo", "dolores", _
        "et", "ea", "rebum", "stet", "clita", "kasd", _
        "gubergren", "no", "sea", "takimata", "sanctus", "est", _
        "lorem", "ipsum"}

    Public Function GetWords(NumWords As Integer) As String
        Dim Result As New StringBuilder()
        Result.Append("lorem ipsum dolor sit amet")

        Dim random As New Random()

        For i As Integer = 0 To NumWords
            Result.Append(" " + words(random.[Next](words.Length - 1)))
        Next

        Result.Append(".")
        Return Result.ToString()
    End Function
End Class