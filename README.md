# Imext Mark VI

Imext Mark VI is the latest iteration of "Imext", Imext Mark VI utilizes the A or Alpha value of a pixel. The Alpha value controls the opacity or transparency of the pixel. Incorporating the 4th value allows for Imext to store 4 chars per pixel instead of 3 in previous versions. 

Updates:
 * Capable of storing 4 chars per pixel
 * Dynamic sizing.
 * Near instant ImageToText / TextToImage conversion.

####  This will most likely be the final version of "Imext", The project is now considered complete and will not be actively worked on.
