﻿Imports System.Text

Public Class Imext

    Public Function ImageToText(sourceFile As String) As String
        Dim sourceBitmap As New Bitmap(sourceFile)
        Dim rectBmp As New Rectangle(0, 0, sourceBitmap.Width, sourceBitmap.Height)
        Dim bmpData As System.Drawing.Imaging.BitmapData = sourceBitmap.LockBits(rectBmp, Imaging.ImageLockMode.ReadWrite, sourceBitmap.PixelFormat)
        Dim bmpPtr As IntPtr = bmpData.Scan0
        Dim bmpTotalBytes As Integer = bmpData.Stride * sourceBitmap.Height
        Dim bmpBytes(bmpTotalBytes - 1) As Byte
        System.Runtime.InteropServices.Marshal.Copy(bmpPtr, bmpBytes, 0, bmpTotalBytes)
        Dim sb As StringBuilder = New StringBuilder()
        Dim data(3) As Byte
        For i As Integer = 0 To bmpBytes.Length - 1 Step 4
            data(0) = bmpBytes(i + 3)
            data(1) = bmpBytes(i + 2)
            data(2) = bmpBytes(i + 1)
            data(3) = bmpBytes(i)
            sb.Append(System.Text.Encoding.Default.GetString(data))
        Next
        System.Runtime.InteropServices.Marshal.Copy(bmpBytes, 0, bmpPtr, bmpTotalBytes)
        sourceBitmap.UnlockBits(bmpData)
        Return sb.ToString()
    End Function 
     
    Function TextToImage(source As String, output As Size) As Image
        Dim retrunImage As New Bitmap(output.Width, output.Height)
        Dim ascList As New List(Of Integer)
        Dim rgbList As New List(Of Color)
        For Each Chr As Char In source
            ascList.Add(Asc(Chr))
        Next
        Do Until ascList.Count Mod 4 = 0
            ascList.Add(0)
        Loop
        For i = 0 To (ascList.Count / 4) - 1
            Dim toAddCol As Color = Color.FromArgb(ascList(i * 4), ascList(i * 4 + 1), ascList(i * 4 + 2), ascList(i * 4 + 3))
            rgbList.Add(toAddCol)
        Next

        Dim x, y As Integer
        For Each col As Color In rgbList
            retrunImage.SetPixel(x, y, col)
            x += 1
            If x = output.Width Then
                x = 0
                y += 1
            End If
        Next
        Return retrunImage
    End Function
End Class